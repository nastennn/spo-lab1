#include "first_mode.c"

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Provide mode number (./lab1 1).\nExecute './lab1 help' for more information.\n");
        return 0;
    }

    char *mode = argv[1];

    if (!strcmp("help", mode)) {
        printf("1 - list disks and partitions\n2 - execute operations on chosen disk, partition or file\n");
        return 0;
    }

    if (!strcmp("1", mode)) {
        return start_first_mode();
    }

    if (!strcmp("2", mode)) {
        if (argc < 3) {
            printf("Provide partition name (./lab1 2 sda3).\n");
            return 1;
        }
        start_second_mode(argv[2]);
        return 0;
    }

    printf("Mode should be one of [1, 2]\n");
    return 0;
}
#include <dirent.h>
#include <string.h>
#include <stdio.h>

#include "second_mode.c"

char *SYS_BLOCK_DIR = "/sys/block/";

int start_first_mode() {
    DIR *sys_block_dir = opendir(SYS_BLOCK_DIR);
    struct dirent *disk;

    if (!sys_block_dir) {
        return 1;
    }


    while ((disk = readdir(sys_block_dir)) != NULL) {
        if (strcmp(disk->d_name, ".") == 0 || strcmp(disk->d_name, "..") == 0) {
            continue;
        }

        printf("/dev/%s\n", disk->d_name);


        char disk_path[256] = {0};
        strcat(strcat(disk_path, SYS_BLOCK_DIR), disk->d_name);
        DIR *sys_block_subdir = opendir(disk_path);
        struct dirent *partition;


        if (!sys_block_subdir) {
            continue;
        }
        while ((partition = readdir(sys_block_subdir)) != NULL) {

            if (!memcmp(partition->d_name, disk->d_name, strlen(disk->d_name))) {
                printf("- /dev/%s\n", partition->d_name);
                struct partition_value *partition_check = open_partition(partition->d_name);
                if (partition_check == NULL) {
                    printf("Not FAT32\n");
                    continue;
                } else {
                    printf("FAT32\n");
                }
                close_partition(partition_check);
            }
        }


        closedir(sys_block_subdir);
    }

    closedir(sys_block_dir);

    return 0;
}

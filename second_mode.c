#include <malloc.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include "helpers.c"
#include "fat32.c"


void execute_ls(struct partition_value *partition) {
    struct file *file = read_dir(partition, partition->active_cluster);

    while (file != NULL) {
        if (file->type == 'd') {
            printf("DIR %s\n", file->filename);
        } else {
            printf("FILE %s (%d bytes)\n", file->filename, file->size);
        }
        file = file->next;
    }

    free_file(file);
}


void copy_file(struct partition_value *part, char *dest, struct file *file) {
    if (file->type != 'f') {
        printf("Not a file\n");
        return;
    }
    char *buf = malloc(part->cluster_size);
    unsigned int fat_record = file->first_cluster;
    int fd = open(dest, O_RDWR | O_APPEND | O_CREAT, 0777);
    unsigned int size = file->size < part->cluster_size ? file->size : part->cluster_size;
    while (fat_record < 0x0FFFFFF7) {
        fat_record = read_file_cluster(part, fat_record, buf);
        write(fd, buf, size);
        unsigned int size_rest = file->size - size;
        if(size_rest > 0){
            size = size_rest;
        }
    }
    free(buf);
    close(fd);
}

void copy_dir(struct partition_value *part, char *dest, struct file *file) {
    if (file->type != 'd') {
        printf("Not a dir\n");
        return;
    }
    struct stat dir = {0};
    if (stat(dest, &dir) == -1) {
        mkdir(dest, 0777);
    }
    struct file *dir_val = read_dir(part, file->first_cluster);
    while (dir_val != NULL) {
        if (strcmp((char *) dir_val->filename, ".") != 0 && strcmp((char *) dir_val->filename, "..") != 0) {
            char *path = calloc(1, 512);
            strcat(path, dest);
            append_path_part(path, (char *) dir_val->filename);
            if (dir_val->type == 'd') {
                copy_dir(part, path, dir_val);
            } else {
                copy_file(part, path, dir_val);
            }
            free(path);
        }
        dir_val = dir_val->next;
    }
    free_file(dir_val);
}

int execute_cd(struct partition_value *partition, char *path)
{
    if (!strcmp("", path))
    {
        printf("You must specify target directory as parameters of 'cd' command\n");
        return -1;
    }

    unsigned int result = change_dir(partition, path);
    if (result == -1) {
        printf("Could not find directory with specified name\n");
        return -1;
    }

    return 0;
}

void execute_cp(struct partition_value *partition, char *arg1, char *arg2){
    struct file *file = read_dir(partition, partition->active_cluster);
    char copied = 0;
    while (file != NULL) {
        if (!strcmp((char*)file->filename, arg1)) {
            if (check_directory(arg2)) {
                char filename[256] = {0};
                strcpy(filename, arg2);
                size_t str_len = strlen(arg2);
                if (filename[str_len - 1] != '/') {
                    strcat(filename, "/");
                }
                strcat(filename, (char *) file->filename);

                if (file->type == 'd') {
                    copy_dir(partition, filename, file);
                } else {
                    copy_file(partition, filename, file);
                }
                copied = 1;
                break;
            } else {
                printf("Directory doesn't exists\n");
            }
        }
        file = file->next;
    }
    if (copied) {
        printf("copied\n");
    } else {
        printf("Dir/file not found\n");
    }
    free_file(file);
}

void print_help() {
    printf("cd [arg] - change working directory\n");
    printf("pwd - print working directory full name\n");
    printf("cp [arg] - copy dir or file to mounted device\n");
    printf("ls - show working directory elements\n");
    printf("exit - terminate program\n");
    printf("help - print help\n");
}

int start_second_mode(const char *part) {
    struct partition_value *partition = open_partition(part);
    if (!partition) {
        printf("Not FAT32 or something bad happened.\n");
        return -1;
    }

    printf("FAT32 partition opened.\n");
    char *current_dir = calloc(1, 512);
    strcat(current_dir, "/");
    strcat(current_dir, part);
    int exit = 0;
    while (!exit) {
        printf("# ");
        char command[256];
        fgets(command, sizeof(command), stdin);

        for (int i = 0; i < sizeof(command); i++) {
            if (command[i] == '\n') {
                command[i] = 0;
                break;
            } else if (command[i] == EOF) {
                return 0;
            }
        }

        char *args[3];
        for (int i = 0; i < 4; i++) {
            args[i] = calloc(1, 256);
        }

        parse(command, args);
        if (!strcmp("ls", args[0])) {
            execute_ls(partition);
        } else if (!strcmp("cp", args[0])) {
            execute_cp(partition, args[1], args[2]);
        } else if (!strcmp("cd", args[0])) {
            int result = execute_cd(partition, args[1]);
            if (result == 0) {
                strcat(current_dir, "/");
                strcat(current_dir, args[1]);
                //check dir .. and delete unused path (0)
            }
        } else if (!strcmp("pwd", args[0])) {
            printf("%s\n", current_dir);
        } else if (!strcmp("help", args[0])) {
            print_help();
        } else if (!strcmp("exit", args[0])) {
            printf("Exiting...\n");
            exit = 1;
        } else if (strlen(args[0]) == 0) {
            // Empty line. Do nothing
        } else {
            printf("You put unknown command. Type \"help\" for list all existing commands\n");
        }

        for (int i = 0; i < 3; i++) {
            free(args[i]);
        }
    }
    close_partition(partition);
}

